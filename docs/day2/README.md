# Introduction
 
This document lists what we've seen on the second day and includes links to expand the knowledge on the subject. 

This is a class diagram of the inheritance package and the classes we used to learn about it.

![Inheritance](./Inheritance.png)

# Covered
- Constructors
- Inheritance
- Method Overriding
- Using Java Libraries
- Looping over a collection

Below is a description of each of the topics covered including the full name of the class containing the examples and the corresponding link to the section or article at the [Java Tutorials](http://docs.oracle.com/javase/tutorial/) which contains information about the subject.
I highly recommend reading through the documentation to learn more about it.

## Constructors

See ```javacourse.day2.constructors.Argentinian```

More information at:

- [Providing Constructors for Your Classes](http://docs.oracle.com/javase/tutorial/java/javaOO/constructors.html)
- [Passing Information to a Method or a Constructor](http://docs.oracle.com/javase/tutorial/java/javaOO/arguments.html)

## Inheritance

See:

- ```javacourse.day2.Person```
- ```javacourse.day2.Argentinian```
- ```javacourse.day2.Dutch```

More information at:

- [Inheritance](http://docs.oracle.com/javase/tutorial/java/IandI/subclasses.html)

## Method overriding

See:

- ```javacourse.day2.Argentinian```
- ```javacourse.day2.Dutch```

More information at:

- [Overriding and Hiding Methods](http://docs.oracle.com/javase/tutorial/java/IandI/override.html)
- [Polymorphism](http://docs.oracle.com/javase/tutorial/java/IandI/polymorphism.html)

## Using Java Libraries

See ```javacourse.day2.PersonDatabase``` particularly the following:

```
import java.util.LinkedList;
```
Which imports the LinkedList class from the Java library to be used by the ```PersonDatabase``` class.

```
LinkedList<Person> persons = new LinkedList<Person>();
```

Which creates a new instance of a ```LinkedList``` class that will contain ```Person``` objects in it.

Then we add ```Person``` objects to the list with ```persons.add(person);```

More information at:

- [Collections](http://docs.oracle.com/javase/tutorial/collections/index.html)

## Looping over a collection

```
for (Person person: persons) {
    person.present();
}
```

This goes over the items in the ```persons``` collection and executes what's between the brackets. 
In this case I named ```person``` the object that represents the current item being iterated.

More information at:

- [For statement](http://docs.oracle.com/javase/tutorial/java/nutsandbolts/for.html)