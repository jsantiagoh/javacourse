package javacourse.day1;

class Colombian {
    String name;
    
    void listenTo(String music) {
        System.out.println("I'm " + name + " and I listen to " + music);
    }
    
    void sayHi(Colombian other) {
        System.out.println("Hi " + other.name + ", I'm " + name);
        listenTo("you");
    }
}
