package javacourse.day1;

public class HelloWorldMainClassThisIsMyEntryPoint {

    public static void main(String[] args) {
        Colombian juan = new Colombian();
        juan.name = "Juan";
        
        Colombian shakira = new Colombian();
        shakira.name = "Shakira";

        juan.listenTo("Bachata");
        shakira.listenTo("Pop crap");
        juan.listenTo("Hardcore");
        
        juan.sayHi(shakira);
    }
}
