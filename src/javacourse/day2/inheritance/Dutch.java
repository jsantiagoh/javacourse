package javacourse.day2.inheritance;

class Dutch extends Person {
    
    void present() {
        System.out.println("I'm " + name + ", and I don't mind much about food");
    }
}
