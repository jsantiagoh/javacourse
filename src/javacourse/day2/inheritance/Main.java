package javacourse.day2.inheritance;

public class Main {

    public static void main(String[] arg) {
        PersonDatabase database = new PersonDatabase();
        
        database.addPerson("Junny");
        database.addArgentinian("Iván");
        database.addDutch("Walter");
        
        database.persentAll();
    }
}
