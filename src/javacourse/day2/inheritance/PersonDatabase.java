package javacourse.day2.inheritance;

import java.util.LinkedList;

class PersonDatabase {
    LinkedList<Person> persons =  new LinkedList<Person>();
    
    void addPerson(String name) {
        Person person = new Person();
        person.name = name;
        persons.add(person);
    }
    
    void addArgentinian(String name) {
        Person person = new Argentinian();
        person.name = name;
        persons.add(person);
    }
    
    void addDutch(String name) {
        Person person = new Dutch();
        person.name = name;
        persons.add(person);
    }
    
    void persentAll() {
        for (Person person: persons) {
            person.present();
        }
    }
}
