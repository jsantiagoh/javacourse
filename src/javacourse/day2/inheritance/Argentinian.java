package javacourse.day2.inheritance;

class Argentinian extends Person {

    void eat() {
        System.out.println("I'm " + name + ", and I like to eat");
    }
    
    void present() {
        eat();
    }
}
