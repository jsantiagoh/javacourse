package javacourse.day2.inheritance;

class Person {
    String name;
    
    void present() {
        System.out.println("Hello, I'm " + name);
    }
}
