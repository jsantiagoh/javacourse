package javacourse.day2.constructors;

public class ConstructorsMain {
    public static void main(String[] args) {
        Argentinian ivan = new Argentinian();
        Argentinian matias = new Argentinian("Matías");
        Argentinian ruben = new Argentinian("Rubén", "Asado");
        Argentinian other = new Argentinian("Other guy", "Salads", "Brasil");
        
        ivan.introduce();
        matias.introduce();
        ruben.introduce();
        other.introduce();
    }
}
