package javacourse.day2.constructors;

class Argentinian {
    String name;
    String favouriteFood;
    String bornIn = "Argentina";
    
    Argentinian() {
        this("Unkown");
    }
    
    Argentinian(String name) {
        this(name, "Everything");
    }
    
    Argentinian(String name, String favouriteFood) {
        this.name = name;
        this.favouriteFood = favouriteFood;
    }
    
    Argentinian(String name, String favouriteFood, String bornIn) {
        this(name, favouriteFood);
        this.bornIn = bornIn;
    }
    
    void introduce() {
        System.out.println(
                "I'm " + name + 
                ", I come from " + bornIn + 
                " and I like " + favouriteFood);
    }
}
